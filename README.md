Domus_r2
=======

Release 2 of domusNode X10/heyu Home Automation Server+Web Client

1) Heyu web interface (assumes heyu installed in the system) (lights control)
2) Boiler temperature control (arduino - attached 2 relays supported)
3) Arduino - based (e.g. YUN) LCD console for status reporting/yahoo weather data display/manual temperature settings


#### For domusNode users

This app is rewritten from scratch

Usage
=======
0) YOur system needs
1) Adjust settings in database/settings/
a) AWAY and HOME profiles
b) globals file - as per comments in each line of the file
c) upload the config files into level db with 'node migrate_ministore2leveldb.js -f'
d) prepare your hardware:
e1) boiler controller - 2 relay board, connected to any arduino w/ethernet (e.g. I use UNO+ethernet) , sketch in /arduino/boiler/
e2) arduino+LCD+temperature sensor+again network board (so YUN or ethernet) display for 'console' showing system status+weather data (optional), 
    sketch in /arduino/console/
e3) optional extra DHT22 sensor for humidity/temp logging , sketch in /arduino/sensor

Careful! You need to modify the arduino sketches to update the IP addresses, etc.
