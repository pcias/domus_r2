
//     URL: http://arduino.cc/playground/Main/DHTLib
//
// Released to the public domain
//

#include <dht.h>
#include <Process.h>

#define LOGFREQ 1800000

#define ACTFREQ 5100
#include "logon.h"

//#define DOMUS_R2_URL "https://domus_r2-c9-pcias.c9.io"
//#define DOMUS_R2_URL "http://pcsbotaniczna.no-ip.org:3000"
#define DOMUS_R2_URL "http://192.168.1.126:3000"
#define SENSOR_ID "YUN2_DHT22"


dht DHT;
#define DHT22_PIN 7



void setup()
{
  // Initialize Bridge
  Bridge.begin();

  // Initialize Serial
  Serial.begin(115200);

  // Wait until a Serial Monitor is connected.
  //while (!Serial);

  Serial.println("DHT INTERFACE FOR DOMUS THERMO ");
  Serial.print("LIBRARY VERSION: ");
  Serial.println(DHT_LIB_VERSION);
  Serial.println();
  Serial.println("Type,\tstatus,\tHumidity (%),\tTemperature (C)");
}

void logTemp(double temp, double humid) {
  Process p;
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter("--data");
  p.addParameter(String("sensor_id=")+SENSOR_ID+"&temperature="+temp+"&humidity="+humid);
  //p.addParameter(String("sensor_id=")+"comment&actual_temperature=23&manual_temperature=20&temperature_source=MAN");
  p.addParameter(String(DOMUS_R2_URL)+"/logger");
  p.run();
}




void loop()
{
  long int m;
   m = millis();
 
  if(!(m % ACTFREQ)) {
  
    // READ DATA
    Serial.print("DHT22, \t");
    int chk = DHT.read22(DHT22_PIN);
    switch (chk)
    {
      case DHTLIB_OK:  
                Serial.print("OK,\t"); 
                break;
      case DHTLIB_ERROR_CHECKSUM: 
                Serial.print("Checksum error,\t"); 
                break;
      case DHTLIB_ERROR_TIMEOUT: 
                Serial.print("Time out error,\t"); 
                break;
      default: 
                Serial.print("Unknown error,\t"); 
                break;
    }
    // DISPLAY DATA
    Serial.print(DHT.humidity, 1);
    Serial.print(",\t");
    Serial.println(DHT.temperature, 1);
  
  }  
  
  //every ca 5 minutes log temperature
  if(!(m % LOGFREQ)) {
    logTemp(DHT.temperature, DHT.humidity);
  }
  
  
  

}
