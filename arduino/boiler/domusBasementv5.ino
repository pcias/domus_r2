/*
domus boilerServer

v5: new watchdog - every 8 second  reset 
*/

#include <SPI.h>
#include <Ethernet.h>
#include <avr/wdt.h>
#include <MemoryFree.h>


// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = { 
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192,168,1,8);  //!!!!set your desired IP address here
const int PORT = 3000; 

const int GLOBAL_PIN = 5;
const int PUMP_PIN = 6;


// Initialize the Ethernet server library
// with the IP address and port you want to use 
EthernetServer server(PORT);

void setup() {
 // Open serial communications and wait for port to open:
  Serial.begin(9600);
   while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  pinMode(GLOBAL_PIN,OUTPUT);
  pinMode(PUMP_PIN,OUTPUT);
  
  // start the Ethernet connection and the server:
  Ethernet.begin(mac, ip);
  server.begin();
  Serial.print("RESET: server is at ");
  Serial.print(Ethernet.localIP());
  Serial.print(":");
  Serial.println(PORT);

  
  
  //client has max 4s to complete command and close connection
  //wdt_enable(WDTO_4S);

  //reset every 8 seconds unless packet was received = winzet works
  wdt_enable(WDTO_8S);
}


void loop() {
  // listen for incoming clients
  EthernetClient client = server.available();
  if (client) {
    String linecom;

    Serial.println("new client");
    // an request ends with a blank line
    boolean currentLineIsBlank = true;
    
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        if(c=='\n') {
          //client has 8 seconds to send command
          wdt_reset();
          executeCommand(linecom);
          client.print("ACK:");
          client.println(linecom);
          client.print("freeMemory()=");
          client.println(freeMemory());
          linecom = "";       
        }
        else  
          linecom+=c;       
          //Serial.write(c);
        }
      }

    // give client time to receive the data
    delay(1);
    // close the connection:
    client.stop();
    Serial.println("client disconnected");
  }
}


void executeCommand(String command) {
  Serial.print("command: ");
  Serial.println(command);
  
  //global on
  if(command=="DO+G1") { 
    digitalWrite(GLOBAL_PIN,LOW);
    Serial.println("domusBasement: GLOBAL ON");
  }
  //global off
  if(command=="DO+G0") { 
    digitalWrite(GLOBAL_PIN,HIGH);
    Serial.println("domusBasement: GLOBAL OFF");
  }
  //pump on
  if(command=="DO+P1") {
    digitalWrite(PUMP_PIN,LOW);
    Serial.println("domusBasement: PUMP ON");
  }
  //pump off
  if(command=="DO+P0") {
    digitalWrite(PUMP_PIN,HIGH);
    Serial.println("domusBasement: PUMP OFF");
  }
  Serial.print("freeMemory()=");
  Serial.println(freeMemory());
}
