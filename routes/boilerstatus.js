
/*
 * switcher of temperature source 
 */

//ministore 
//    var Store = require('ministore')('./database/settings');
var levelup =  require('levelup');


//returns json { boilier_connection_ok : true|false }
exports.get = function(req, res){
    //var globalsStore = Store('globals');
    var globalsdb = levelup('./leveldb/globals', { valueEncoding : 'json' });
    globalsdb.get('boiler_connection_ok', function(err,data) { res.json(data); globalsdb.close(); }) ;
    return;
};

exports.heating = function(req,res) {
//    var globalsStore = Store('globals');    
    var globalsdb = levelup('./leveldb/globals', { valueEncoding : 'json' });
    globalsdb.get('heating', function(err,data) { res.json(data); globalsdb.close(); }) ;
    return;
}

exports.global = function(req,res) {
//    var globalsStore = Store('globals');    
    var globalsdb = levelup('./leveldb/globals', { valueEncoding : 'json' });
    globalsdb.get('global', function(err,data) { res.json(data); globalsdb.close(); }) ;
    return;
}