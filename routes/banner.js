
/*
 * banner provider for arduino LCD
 */

//ministore 
// var newsStore = require('ministore')('./database/news');
var settingsStore = require('ministore')('./database/settings');
var config = settingsStore('config');

var levelup =  require("levelup");
var globalsdb = levelup('./leveldb/globals', { valueEncoding : 'json' });



var tsession = require("temboo/core/temboosession");
var session = new tsession.TembooSession(config.get("temboo_USER"), config.get("temboo_APPLICATION_NAME"), config.get("temboo_APPKEY"));

//xml parsing required to read yahoo data
var select = require('xpath.js')
    , dom = require('xmldom').DOMParser;


var i = 0; //round robin


//returns banner
exports.get = function(req, res){
    // var bannersStore = newsStore('banner');
    // var banners =  bannersStore.get('banner');
    var bannersdb = levelup('./leveldb/banner', { valueEncoding : 'json' });
    bannersdb.get('banner', function(err,data) { res.send(data[ (i++) % data.length ]); bannersdb.close();})
    return;
};



//read Temboo Yahoo- weather and populate banner
exports.populate = function() {
    var Yahoo = require("temboo/Library/Yahoo/Weather");
    var getWeatherByAddressChoreo = new Yahoo.GetWeatherByAddress(session);
    var globalsdb = levelup('./leveldb/globals', { valueEncoding : 'json'});
    var bannersdb = levelup('./leveldb/banner', { valueEncoding : 'json' });
    // Instantiate and populate the input set for the choreo
    var getWeatherByAddressInputs = getWeatherByAddressChoreo.newInputSet();

    
    // Set inputs
    getWeatherByAddressInputs.set_Units("c");
    config.get('address' , 
        function(err,address) { 
            getWeatherByAddressInputs.set_Address(address);
            // Run the choreo, specifying success and error callback handlers
            getWeatherByAddressChoreo.execute(
            getWeatherByAddressInputs,
            function(resultsxml){ 
                    var banners = newsStore('banner');
                    var bannerArray =  new Array();
            
                    var doc = new dom().parseFromString(resultsxml.get_Response()); 
                    //temperature
                    var nodes = select(doc, "/rss/channel/item/yweather:condition/@temp");
                    bannerArray[0] = "Outside "+nodes[0].nodeValue+" C";
                    globalsdb.put('outside_temperature',parseInt(nodes[0].nodeValue));
                
                    //weather description
                    nodes = select(doc, "/rss/channel/item/yweather:condition/@text");
                    bannerArray[1] = nodes[0].nodeValue;
                
                    //forecast for today
                    var nodes_day = select(doc,"/rss/channel/item/yweather:forecast/@day");
                    var nodes_low = select(doc,"/rss/channel/item/yweather:forecast/@low");
                    var nodes_high= select(doc,"/rss/channel/item/yweather:forecast/@high");
                    var nodes_text = select(doc,"/rss/channel/item/yweather:forecast/@text");
                    var index = 2;
                    for(var i = nodes_day.length-1; /*i>=0*/ i >= nodes_day.length-2 ; i--) {
                        var day = nodes_day[i].nodeValue;
                        var low = nodes_low[i].nodeValue;
                        var high = nodes_high[i].nodeValue;
                        var text = nodes_text[i].nodeValue;
                        bannerArray[index++] = day+' '+low+'-'+high+' '+text;
                    }
                    bannersdb.put('banner',bannerArray);
                    },
                    function(error){console.log(error.type); console.log(error.message);}
                );
        });
}
