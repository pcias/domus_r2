
/*
 * GET home page.
 */



exports.index = function(req, res){
  var heyuController = require('heyu');
  var Store = require('ministore')('./database/settings');
  var configStore = Store('config');
  var levelup = require('levelup');
  var hoursdb = levelup('./leveldb/'+configStore.get('profile'), { valueEncoding : 'json' });
  var globalsdb = levelup('./leveldb/globals', { valueEncoding : 'json' });
  
  heyuController.readConfigFile(configStore.get('x10conf'),render_fun);
 
  
   function render_fun(x10grouped) {    
//     var thermoset = hoursStore.all();  
//     console.log(JSON.stringify(thermoset));
//     thermoset = hoursdb.list();
//     console.log(JSON.stringify(thermoset));
     
     var storeArray = [];
     hoursdb.createReadStream()
        .on('data', function (data) {
            console.log(data.key, '=', data.value);
            var newItem = {};
            newItem["hour"] = data.key; 
            newItem["temp"] = data.value.temp;
            newItem["water"]  = data.value.water;
            storeArray.push(newItem); 
            })
        .on('error', function (err) {
            //console.log('Oh my!', err)
            })
        .on('close', function () {
            //console.log('Stream closed')
        })
        .on('end', function () {
            //console.log('Stream closed')
        })
     
     
     console.log(JSON.stringify(storeArray));
     
     
     globalsdb.get('boiler_connection_ok', function (err,data) {    
                     res.render('index.hjs',  { 
                                                 groups : x10grouped,
                                                 hours : storeArray,
                                                 boiler_connection_ok : data
                                           }
                                           );
                    globalsdb.close();                       
                    });
   };
};