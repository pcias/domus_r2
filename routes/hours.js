
/*
 * REST hours setting.
 */

 //ministore with temperature setting per hour
var Store = require('ministore')('./database/settings');
var levelup =  require("levelup");
var globalsdb = levelup('./leveldb/globals', { valueEncoding : 'json' });

   



//expected to receive one hour input { hour : "0" ,  temp : "12" }
exports.write = function(req, res){
//  var configStore = Store('globals');
//  var hoursStore = Store(globalsStore.get('profile'));

  
  globalsdb.get('profile', function(err,profile) {
    var hoursdb = levelup('./leveldb/'+profile, { valueEncoding : 'json' });
     
    var newObj = {};    
    newObj['temp'] = req.body.temp;
  
    //unfortunately need to convert string to bool
    if (req.body.water=='true')
        newObj['water'] = true;
    else
        newObj['water'] = false;
    
    console.log(newObj);
    hoursdb.put(req.body.hour,newObj, function(err) { hoursdb.close()})
    
    globalsdb.close();
    res.json(true);
    
  });
  
};

exports.close = function (err, callback) {
    globalsdb.close(err,callback);
}