/*
 *  logger (neverending)
 */

//ministore 
var Store = require('ministore')('./database/log');
var logStore = Store('log');
var sensStore = require('ministore')('./database/settings');
var sensorsStore = sensStore('sensors');

var Spreadsheet = require('edit-google-spreadsheet');



//expected to receive  req = { /*channel name*/ "temperature" : "21.0", "humidity" : "22.0" , "sensor_id" = "sensor" }
exports.write = function(req, res){

    
     // validate data
    if(!req.body.sensor_id) {
        throw new Error('domus_humidity:  Sensor data (POST) - wrong format');
        res.json(false);
        return;
    }
    
    
    sensorsStore.get(req.body.sensor_id, process_sensor);
    
    function process_sensor(err,sensor) {
        //everything ok
        if(err) {
            throw new Error('humidity-js: no such sensor in config?');
        }
        
        var date = new Date();
        var record = new Object();
        
        for(var key in req.body) {
                record[key] = req.body[key];
        } 
       
 
        Spreadsheet.create({
            debug: true,
            oauth : {
                email: sensor.googleSpreadsheetLogging.email,
                keyFile: sensor.googleSpreadsheetLogging.keyFile
            },
            spreadsheetName: sensor.googleSpreadsheetLogging.spreadsheetName,
            worksheetName: sensor.googleSpreadsheetLogging.worksheetName,
            callback: sheetReady
        });
        
        res.json(true);
        return logStore.set(date,record);
    
    
        function sheetReady(err, spreadsheet) {
            if(err) throw err;

            spreadsheet.receive(function(err, rows, info) {
                if(err) throw err;
                //console.log("Found rows:", rows);
                console.log("info:", info);
                var newRow =  {};
                var date = new Date();
                /* date/time columns */
                newRow[info.nextRow] =  { 1: String(date), 2: date.getDay() , 3: date.getFullYear() , 4: parseInt(date.getMonth())+1, 
                                        5: date.getDate(), 6: date.getHours(), 7: date.getMinutes(), 8: date.getTime() , 
                                        10: date.getFullYear()+"-"+String(parseInt(date.getMonth())+1)+"-"+date.getDate()+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds(),
                                        9: record["sensor_id"] }; 
                                /* 10: actualTemp, 11:actualHumid, 12: req.body.sensor_id }; */
                /* data columns as defined in sensors settings file */
                for(var measurement in record) {
                    if(measurement!=="sensor_id") { 
                      if(sensor[measurement]) {
                        newRow[info.nextRow][sensor[measurement].column] = record[measurement];
                      }
                      else
                        throw new Error('humidity-js: cannot read sensor config file at '+sensor[measurement]);
                    }
                }                             
                spreadsheet.add( newRow );
      
                spreadsheet.send(function(err) {
                    if(err) throw err;
                        console.log("Spreadsheet updated.");
                    });
                })
        };

    // spreadsheet.send(function(err) {
    //   if(err) throw err;
    //   console.log("Updated Cell at row 3, column 5 to 'hello!'");
    // });
  }

    
};


