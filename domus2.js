
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes'),
    hours = require('./routes/hours'),
    heyu = require('./routes/heyu'),
    templogger = require('./routes/templogger'),
    logger = require('./routes/logger'),
    boilerstatus = require('./routes/boilerstatus'),
    banner = require('./routes/banner'),
//    homeaway = require('./routes/homeaway'),
    settings =  require('./routes/settings');
var http = require('http');
var path = require('path');

//user authentication
var user = require('./modules/user');

var app = express();


// all environments
app.set('port', process.env.PORT || 3000);
//app.set('address', process.env.IP || '127.0.0.1');
app.set('views', __dirname + '/views');
app.set('view engine', 'hjs');
app.set('title','domus_R2');



app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
app.use(express.session());


app.use(express.static(path.join(__dirname, 'public')));

// Authenticated paths
app.use(express.basicAuth(function(username, password, callback) {
 user.authenticate(username,password,callback);
}));
app.use(app.router);





// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}




app.get('/', routes.index);
app.get('/boilerstatus', boilerstatus.get);
app.get('/heating', boilerstatus.heating);
app.get('/global', boilerstatus.global);


//display banner on arduino
app.get('/banner',banner.get);

//pupulate banner with weather data  
app.get('/populate',banner.populate);

//hourly temp schedule
app.post('/hours', hours.write);

//heyu lights on/off
app.post('/heyu', heyu.exec);

//temperature logging
app.post('/templogger',templogger.write);

//temperature logging v2
app.post('/logger',logger.write);

//operations on settings/globals
app.post('/settings', settings.write);
app.get('/web_temperature',settings.get_web_temperature);
app.get('/manual_temperature',settings.get_manual_temperature);
app.get('/actual_temperature',settings.get_actual_temperature);
app.get('/temperature_source',settings.get_temperature_source);
app.get('/profile',settings.get_profile);

//home/away screen
//app.get('/homeaway',homeaway.get);
//app.post('/homeway',homeaway.write);



http.createServer(app).listen(app.get('port'), function(){
  console.log('domusR2 Express server listening on port ' + app.get('port'));
});


var loop = require('./modules/loop');
setInterval(loop.loop,5000);
//read web api once an hour
setInterval(banner.populate,3600*1000);
  



