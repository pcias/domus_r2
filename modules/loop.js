//cron-like or processing's loop



var Store = require('ministore')('./database/settings');
var net = require('net');
var heating, global;
var async = require("async");
var levelup =  require("levelup");




function boilerConnect(boilerCmd){
    var configStore = Store('config');
    var globalsdb = levelup('./leveldb/globals', { valueEncoding : 'json' });
    //if testing env
    var boilerIP, boilerPORT;
    
    async.series([
            //read config from database
            function(callback) {
                configStore.get('boilerIP', function (err,value) { 
                                if(err) return callback(err);
                                boilerIP = value;
                                callback();
                        });
            },
            function(callback) {
                configStore.get('boilerPORT', function (err,value) { 
                                if(err) return callback(err);
                                boilerIP = value;
                                callback();
                        });
            },    
            //connect to the boiler      
            function(callback) {
                if(boilerIP == '0.0.0.0') { //test 
                    callback();
                    return;
                }
                //otherwise production
                var connection = net.connect(boilerPORT, boilerIP, function() {
                    var date = new Date();
                    console.log(date.toString()+'domus_r2: connection to arduino SUCCESS');
                    connection.write(boilerCmd);
                    globalsdb.put('boiler_connection_ok',true);
                });
                connection.on('error', function() {
                    var date = new Date();
                    console.log(date.toString()+'domus_r2: connection to arduino ERROR');
                    globalsdb.put('boiler_connection_ok',false);
                });
                connection.on('data', function(data) {
                console.log('domus_r2: arduino response: '+data.toString());
                globalsdb.put('boiler_connection_ok',true, function() { globalsdb.close(); });
                connection.end();
                });
            }
        ])
}


function heatingOn(){
    var globalsdb = levelup('./leveldb/globals', { valueEncoding : 'json' });
    globalsdb.put("heating",true, function () { globalsdb.close();});
    heating =  true;
    console.log("heating on");
}

function heatingOff(){
    var globalsdb = levelup('./leveldb/globals', { valueEncoding : 'json' });
    heating =  false;
    globalsdb.put("heating",false, function () { globalsdb.close();});
    console.log("heating off");
}

function waterOn(){
    var globalsdb = levelup('./leveldb/globals', { valueEncoding : 'json' });
    global =  true;
    globalsdb.put("global",true, function () { globalsdb.close();});
    console.log("global on");
}

function waterOff(){
    var globalsdb = levelup('./leveldb/globals', { valueEncoding : 'json' });
    global =  false;
    globalsdb.put("global",false, function () { globalsdb.close();});
    console.log("global off");
}



function loop() {

  var date = new Date();
  var configStore = Store('config');
  var globalsdb = levelup('./leveldb/globals', { valueEncoding : 'json' });
//  var hoursStore = Store(globalsStore.get('profile'));
  var hours = date.getHours();
  var act ; //actual temperature
  var st; //manual (set) temperature
  var profile;
  var ts; //temperature source
  var hoursdb;
  var wtr; //web temperature record = temperatue from hoursdb
  

  function actuate(act,st) {
         //a little hysteresis
         if(act < st - 0.5) {
              heatingOn();
          }
          else if (act >= st + 0.5 ) {
              heatingOff();    
          }
          //needs repeating boiler commands in case arduino resets
          if(global) 
            configStore.get("waterOnCmd", function(err,cmd) {boilerConnect(cmd);});
          else 
            configStore.get("waterOffCmd", function(err,cmd) {boilerConnect(cmd);});
          
          if(heating) 
            configStore.get("heatingOnCmd", function(err,cmd) {boilerConnect(cmd);});
          else 
            configStore.get("heatingOffCmd", function(err,cmd) {boilerConnect(cmd);});
  }


  
  async.series([
        function(callback) {
            var globalsdb = levelup('./leveldb/globals', { valueEncoding : 'json' });
            globalsdb.get('profile', function(err,value) { profile =  value;  clocallback()})
        },
        function(callback) {
            globalsdb.get('temperature_source', function(err,value) { ts = value ; callback()});
        },
        function(callback) {
            hoursdb  = levelup('./leveldb/'+profile, { valueEncoding : 'json' });
            hoursdb.get(hours, function(err,value) { wtr = value; callback() });
        },
        function(callback) {
                if(ts=="MAN") {
                    globalsdb.get('manual_temperature', function(err,value) { st = parseInt(value) ; callback()});
                }
                if(ts=="WEB") {
                    st = parseInt(wtr.temp);
                    globalsdb.put('web_temperature',st);
                    callback();
                }
        },
        function(callback) {
            globalsdb.get('actual_temperature', function(err,value) { act = parseFloat(value); callback()});
        }
        
      ],
      function(err,results) {
                globalsdb.close();
                actuate(act,st);
                //water always comes from WEB
                if(wtr.water) {
                    waterOn();
                }
                else {
                    waterOff();
                }
        }
      );
}

exports.loop =  loop;
